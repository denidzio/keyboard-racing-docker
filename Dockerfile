FROM node:12-alpine

COPY src ./src
COPY *.json ./

RUN npm install

CMD ["npm", "run", "start"]