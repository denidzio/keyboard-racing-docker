import { createServer } from "http";
import path from "path";
import express from "express";
import cors from "cors";
import { Server } from "socket.io";

import { PORT, STATIC_PATH } from "./config";
import { SERVER_CONFIG } from "./socket/config";

import KeyboardRacingFacade from "./keyboardRacing/KeyboardRacingFacade";
import BotFacade from "./bot/BotFacade";

import routes from "./routes";

const app = express();
const httpServer = createServer(app);

app.use(cors());
app.use(express.static(STATIC_PATH));

routes(app);

const io = new Server(httpServer, SERVER_CONFIG);

const keyboardRacing = new KeyboardRacingFacade(io);
const bot = new BotFacade(io);

app.get("*", (req, res) => {
  res.sendFile(path.join(STATIC_PATH, "index.html"));
});

keyboardRacing.listen();
bot.listen();

httpServer.listen(process.env.PORT || PORT, () => {
  console.log(`Server has been launched on port ${process.env.PORT || PORT}`);
});

export { app, httpServer };
