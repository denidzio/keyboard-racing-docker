import ISocketHandleOptions from "./ISocketHandlerOptions";

export default interface ISocketHandler {
  handle(options: ISocketHandleOptions): void;
}
