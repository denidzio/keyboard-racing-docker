import { Server, Socket } from "socket.io";
import { DefaultEventsMap } from "socket.io/dist/typed-events";

export default interface ISocketHandlerOptions {
  socket?: Socket<DefaultEventsMap, DefaultEventsMap>;
  io?: Server<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap>;
  param?: any;
}
