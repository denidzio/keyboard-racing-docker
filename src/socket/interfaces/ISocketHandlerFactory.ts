import SocketHandlerType from "../SocketHandlerType";
import { ISocketHandler } from "./";

export default interface ISocketHandlerFactory {
  create(type: any): ISocketHandler | null;
}
