interface IUser {
  username: string;
  currentRoomName?: string;
}

export default IUser;
