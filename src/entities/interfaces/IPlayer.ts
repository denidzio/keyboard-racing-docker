import { IUser } from "./";

interface IPlayer {
  user: IUser;
  isReady?: boolean;
  inputtedText?: string;
  progress?: number;
  finishTime?: number;
}

export default IPlayer;
