import { IPlayer } from "./";

interface IRoom {
  name: string;
  players: IPlayer[];
  hasBeenStartedAt?: number;
  textId?: number;
  timerId?: NodeJS.Timeout;
}

export default IRoom;
