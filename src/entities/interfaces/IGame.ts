interface IGame {
  textId: number;
  timerBeforeGame?: number;
  timerDuringGame?: number;
}

export default IGame;
