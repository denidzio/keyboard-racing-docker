import {
  SECONDS_FOR_GAME,
  SECONDS_TIMER_BEFORE_START_GAME,
} from "../socket/config";

import { IGame } from "./interfaces";

class Game implements IGame {
  public textId: number;
  public timerBeforeGame: number = SECONDS_TIMER_BEFORE_START_GAME;
  public timerDuringGame: number = SECONDS_FOR_GAME;

  constructor({ timerBeforeGame, timerDuringGame, textId }: IGame) {
    this.textId = textId;

    if (timerDuringGame) {
      this.timerDuringGame = timerDuringGame;
    }

    if (timerBeforeGame) {
      this.timerBeforeGame = timerBeforeGame;
    }
  }
}

export default Game;
