import { IUser } from "./interfaces";

class User implements IUser {
  public username: string = "Unknown";
  public currentRoomName?: string | undefined;

  constructor({ username, currentRoomName }: IUser) {
    this.username = username;
    this.currentRoomName = currentRoomName;
  }
}

export default User;
