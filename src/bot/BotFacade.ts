import { Server } from "socket.io";
import { DefaultEventsMap } from "socket.io/dist/typed-events";
import { getDb } from "../dao/roomDao";
import { IRoom } from "../entities/interfaces";
import { getOneRoom } from "../services/roomService/roomCrudService";
import BotHandlerFactory from "./BotHandlerFactory";
import BotHandlerType from "./BotHandlerType";

//FACADE
class BotFacade {
  public io: Server<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap>;

  constructor(
    io: Server<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap>
  ) {
    this.io = io;
  }

  public listen() {
    const rooms = getDb();

    rooms.observe((key, room: IRoom) => {
      const io = this.io;
      const oldRoom = getOneRoom(room.name);

      if (!oldRoom) {
        return;
      }

      const handlerFactory = new BotHandlerFactory();

      const startGameHandler = handlerFactory.create(BotHandlerType.START_GAME);
      const endGameHandler = handlerFactory.create(BotHandlerType.END_GAME);
      const duringGameHandler = handlerFactory.create(
        BotHandlerType.DURING_GAME
      );

      if (!startGameHandler || !endGameHandler || !duringGameHandler) {
        return;
      }

      if (!oldRoom.hasBeenStartedAt && room.hasBeenStartedAt) {
        startGameHandler.handle({ io, param: room });
        return;
      }

      if (oldRoom.hasBeenStartedAt && !room.hasBeenStartedAt) {
        endGameHandler.handle({ io, param: oldRoom });
        return;
      }

      if (!room.hasBeenStartedAt) {
        return;
      }

      duringGameHandler.handle({ io, param: { oldRoom, newRoom: room } });
    });
  }
}

export default BotFacade;
