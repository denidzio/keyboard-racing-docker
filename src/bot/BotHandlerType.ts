enum BotHandlerType {
  DURING_GAME,
  END_GAME,
  START_GAME,
  STATS,
}

export default BotHandlerType;
