import { SECOND } from "../../constants";
import { IPlayer, IRoom } from "../../entities/interfaces";
import { getGameResults } from "../../services/gameService/gameSimpleService";
import { getPlayerTime } from "../../services/playerService/playerSimpleService";
import { ISocketHandler, ISocketHandlerOptions } from "../../socket/interfaces";

class EndGameHandler implements ISocketHandler {
  public handle({ io, param: room }: ISocketHandlerOptions): void {
    if (!io || !room) {
      return;
    }

    io.to(room.name).emit(
      "SEND_COMMENT",
      `Game over!\n${this.getResultString(room)}`
    );
  }

  private getResultString(room: IRoom | undefined) {
    if (!room || !room.hasBeenStartedAt) {
      return;
    }

    let string = "";
    const stats = getGameResults(room);

    if (!stats) {
      return;
    }

    if (stats[0]) {
      string += `1. ${stats[0].user.username} with result ${this.getTimeString(
        room,
        stats[0]
      )}`;
    }

    if (stats[1]) {
      string += `\n2. ${
        stats[1].user.username
      } with result ${this.getTimeString(room, stats[1])}`;
    }

    if (stats[2]) {
      string += `\n3. ${
        stats[2].user.username
      } with result ${this.getTimeString(room, stats[2])}`;
    }

    return string;
  }

  private getTimeString(room: IRoom | undefined, player: IPlayer | undefined) {
    if (!room || !player) {
      return;
    }

    return (getPlayerTime(room, player) / SECOND).toFixed(1) + "s";
  }
}

export default EndGameHandler;
