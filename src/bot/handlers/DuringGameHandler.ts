import { IPlayer } from "../../entities/interfaces";
import { isProgressChanged } from "../../services/gameService/gameSimpleService";
import { ISocketHandler, ISocketHandlerOptions } from "../../socket/interfaces";
import { SYMBOLS_BEFORE_FINISH } from "../config";

class DuringGameHandler implements ISocketHandler {
  handle({ io, param: { oldRoom, newRoom } }: ISocketHandlerOptions): void {
    if (!io || !oldRoom || !newRoom) {
      return;
    }

    newRoom.players.forEach((player: IPlayer, index: number) => {
      if (!player.inputtedText || !player.progress) {
        return;
      }

      const symbolsToFinish = Math.round(
        player.inputtedText.length * (100 / player.progress - 1)
      );

      if (
        symbolsToFinish === SYMBOLS_BEFORE_FINISH &&
        isProgressChanged(oldRoom, newRoom, player.user.username)
      ) {
        io.to(newRoom.name).emit(
          "SEND_COMMENT",
          `Ohh! ${player.user.username} has ${SYMBOLS_BEFORE_FINISH} characters to Finish`
        );
      }

      if (
        symbolsToFinish === 0 &&
        isProgressChanged(oldRoom, newRoom, player.user.username)
      ) {
        io.to(newRoom.name).emit(
          "SEND_COMMENT",
          `Yeaah! ${player.user.username} is on Finish!`
        );
      }
    });
  }
}

export default DuringGameHandler;
