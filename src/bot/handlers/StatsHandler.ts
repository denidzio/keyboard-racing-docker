import { SECOND } from "../../constants";
import { getOne } from "../../dao/roomDao";
import { IPlayer } from "../../entities/interfaces";
import { getGameResults } from "../../services/gameService/gameSimpleService";
import { ISocketHandler, ISocketHandlerOptions } from "../../socket/interfaces";
import { TIME_BETWEEN_MESSAGES } from "../config";
import { getPlayersAsString } from "../../services/playerService/playerSimpleService";

class StatsHandler implements ISocketHandler {
  handle({ io, param: roomName }: ISocketHandlerOptions): void {
    if (!io || !roomName) {
      return;
    }

    const room = getOne(roomName);

    if (!room) {
      return;
    }

    if (!room.hasBeenStartedAt) {
      return;
    }

    const stats = getGameResults(room);

    if (!stats) {
      return;
    }

    io.to(roomName).emit("SEND_COMMENT", this.getStatMessage(stats));

    setTimeout(
      () => this.handle({ io, param: roomName }),
      TIME_BETWEEN_MESSAGES * SECOND
    );
  }

  private getStatMessage = (results: IPlayer[] | undefined) => {
    if (!results) {
      return;
    }

    return `Hmm. At the moment we have next:\n${getPlayersAsString(results)}`;
  };
}

export default StatsHandler;
