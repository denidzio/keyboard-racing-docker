import { SECOND } from "../../constants";
import { getPlayersAsString } from "../../services/playerService/playerSimpleService";
import { SECONDS_TIMER_BEFORE_START_GAME } from "../../socket/config";
import { ISocketHandler, ISocketHandlerOptions } from "../../socket/interfaces";
import BotHandlerFactory from "../BotHandlerFactory";
import BotHandlerType from "../BotHandlerType";
import { TIME_BETWEEN_MESSAGES } from "../config";

class StartGameHandler implements ISocketHandler {
  public handle({ io, param: room }: ISocketHandlerOptions) {
    if (!io || !room) {
      return;
    }

    io.to(room.name).emit(
      "SEND_COMMENT",
      "Greetings! My name is SuperBot3000 and today I'll comment the upcoming game"
    );

    setTimeout(() => {
      io.to(room.name).emit(
        "SEND_COMMENT",
        `So, our participants:\n${getPlayersAsString(room.players)}`
      );
    }, (SECONDS_TIMER_BEFORE_START_GAME / 2) * SECOND);

    const handlerFactory = new BotHandlerFactory();
    const statsHandler = handlerFactory.create(BotHandlerType.STATS);

    if (!statsHandler) {
      return;
    }

    setTimeout(() => {
      statsHandler.handle({ io, param: room.name });
    }, (SECONDS_TIMER_BEFORE_START_GAME + TIME_BETWEEN_MESSAGES) * SECOND);
  }
}

export default StartGameHandler;
