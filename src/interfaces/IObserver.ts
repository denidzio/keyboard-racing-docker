interface IObserver {
  observe(func: (key: string, string: any) => void): void;
}

export default IObserver;
