enum KeyboardRacingHandlerType {
  USER_CONNECT = "USER_CONNECT",
  ADD_ROOM = "ADD_ROOM",
  JOIN_TO_ROOM = "JOIN_TO_ROOM",
  TOGGLE_PLAYER_STATE = "TOGGLE_PLAYER_STATE",
  ROOM_EXIT = "ROOM_EXIT",
  INPUT_CHAR = "INPUT_CHAR",
  USER_DISCONNECT = "disconnect",
  START_GAME = "START_GAME",
  GAME_OVER = "GAME_OVER",
  LEAVE_ROOM_BEFORE_GAME = "LEAVE_ROOM_BEFORE_GAME",
  LEAVE_ROOM_DURING_GAME = "LEAVE_ROOM_DURING_GAME",
}

export default KeyboardRacingHandlerType;
