import { getOneRoom } from "../../services/roomService/roomCrudService";
import {
  getAvailableRooms,
  removeUserFromRoom,
} from "../../services/roomService/roomSimpleService";
import {
  getOneUser,
  updateUser,
} from "../../services/userService/userCrudService";
import { ISocketHandler, ISocketHandlerOptions } from "../../socket/interfaces";
import KeyboardRacingHandlerFactory from "../KeyboardRacingHandlerFactory";
import KeyboardRacingHandlerType from "../KeyboardRacingHandlerType";

class LeaveRoomHandler implements ISocketHandler {
  public handle({ io, socket }: ISocketHandlerOptions): void {
    if (!io || !socket) {
      return;
    }

    const user = getOneUser(socket.handshake.query.username as string);

    if (!user || !user.currentRoomName) {
      return;
    }

    const room = getOneRoom(user.currentRoomName);

    if (!room) {
      return;
    }

    const roomWithoutUser = removeUserFromRoom(room, user);

    if (!roomWithoutUser) {
      return;
    }

    const handlerFactory = new KeyboardRacingHandlerFactory();

    if (room.hasBeenStartedAt) {
      const leaveRoomDuringGameHandler = handlerFactory.create(
        KeyboardRacingHandlerType.LEAVE_ROOM_DURING_GAME
      );

      const param = { user, room: roomWithoutUser };

      leaveRoomDuringGameHandler?.handle({
        socket,
        param,
      });
    } else {
      const leaveRoomBeforeGameHandler = handlerFactory.create(
        KeyboardRacingHandlerType.LEAVE_ROOM_BEFORE_GAME
      );

      const param = { user, room: roomWithoutUser };

      leaveRoomBeforeGameHandler?.handle({
        io,
        socket,
        param,
      });
    }

    updateUser({ ...user, currentRoomName: undefined });

    socket.leave(room.name);
    socket.emit("UPDATE_ROOMS", getAvailableRooms());
    socket.broadcast.emit("UPDATE_ROOMS", getAvailableRooms());
  }
}

export default LeaveRoomHandler;
