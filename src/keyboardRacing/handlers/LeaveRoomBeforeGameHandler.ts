import { mapRoomForClient } from "../../helpers/room.helper";
import { removeRoom } from "../../services/roomService/roomCrudService";
import {
  isRoomReady,
  removeUserFromRoom,
} from "../../services/roomService/roomSimpleService";
import { ISocketHandler, ISocketHandlerOptions } from "../../socket/interfaces";
import KeyboardRacingHandlerFactory from "../KeyboardRacingHandlerFactory";
import KeyboardRacingHandlerType from "../KeyboardRacingHandlerType";

class LeaveRoomBeforeGameHandler implements ISocketHandler {
  handle({ io, socket, param: { room, user } }: ISocketHandlerOptions): void {
    if (!io || !socket || !room || !user) {
      return;
    }

    if (room.players.length === 0) {
      removeRoom(room.name);
      return;
    }

    socket.to(room.name).emit("UPDATE_ROOM", mapRoomForClient(room));

    if (isRoomReady(room)) {
      const handlerFactory = new KeyboardRacingHandlerFactory();
      const startGameHandler = handlerFactory.create(
        KeyboardRacingHandlerType.START_GAME
      );

      startGameHandler?.handle({ io, param: room });
    }
  }
}

export default LeaveRoomBeforeGameHandler;
