import { ISocketHandler, ISocketHandlerOptions } from "../../socket/interfaces";

import { Player, Room, User } from "../../entities";

import { getAvailableRooms } from "../../services/roomService/roomSimpleService";
import { createRoom } from "../../services/roomService/roomCrudService";

import {
  getOneUser,
  updateUser,
} from "../../services/userService/userCrudService";

class AddRoomHandler implements ISocketHandler {
  public handle({ socket, param: name }: ISocketHandlerOptions): void {
    if (!name || !socket) {
      return;
    }

    const user = getOneUser(socket.handshake.query.username as string);

    if (!user) {
      return;
    }

    const updatedUser = new User({ ...user, currentRoomName: name });

    const newPlayer = new Player({
      user: updatedUser,
    });

    const newRoom = createRoom(new Room({ name, players: [newPlayer] }));

    if (!newRoom) {
      socket.emit("ROOM_NAME_IS_BUSY");
      return;
    }

    socket.join(newRoom.name);
    socket.emit("JOIN_TO_ROOM", newRoom);
    socket.broadcast.emit("UPDATE_ROOMS", getAvailableRooms());

    updateUser(updatedUser);
  }
}

export default AddRoomHandler;
