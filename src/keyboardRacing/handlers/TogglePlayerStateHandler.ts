import { ISocketHandler, ISocketHandlerOptions } from "../../socket/interfaces";

import { getOneUser } from "../../services/userService/userCrudService";

import { getOneRoom } from "../../services/roomService/roomCrudService";
import {
  isRoomReady,
  updatePlayerInRoom,
} from "../../services/roomService/roomSimpleService";

import { getPlayerByUser } from "../../services/playerService/playerSimpleService";
import KeyboardRacingHandlerFactory from "../KeyboardRacingHandlerFactory";
import KeyboardRacingHandlerType from "../KeyboardRacingHandlerType";
import { Player } from "../../entities";

class TogglePlayerStateHandler implements ISocketHandler {
  public handle({ io, socket }: ISocketHandlerOptions): void {
    if (!io || !socket) {
      return;
    }

    const user = getOneUser(socket.handshake.query.username as string);

    if (!user || !user.currentRoomName) {
      return;
    }

    const player = getPlayerByUser(user);
    const room = getOneRoom(user.currentRoomName);

    if (!player || !room) {
      return;
    }

    const newPlayer = new Player({ ...player, isReady: !player.isReady });
    const updatedRoom = updatePlayerInRoom(user.currentRoomName, newPlayer);

    if (!updatedRoom) {
      return;
    }

    io.to(updatedRoom.name).emit("UPDATE_ROOM", updatedRoom);

    if (isRoomReady(updatedRoom)) {
      const handlerFactory = new KeyboardRacingHandlerFactory();
      const startGameHandler = handlerFactory.create(
        KeyboardRacingHandlerType.START_GAME
      );

      startGameHandler?.handle({ io, param: updatedRoom });
    }
  }
}

export default TogglePlayerStateHandler;
