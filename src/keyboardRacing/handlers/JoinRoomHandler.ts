import { ISocketHandler, ISocketHandlerOptions } from "../../socket/interfaces";

import { User } from "../../entities";

import {
  addUserToRoom,
  getAvailableRooms,
  isAvailableRoom,
} from "../../services/roomService/roomSimpleService";
import { getOneRoom } from "../../services/roomService/roomCrudService";

import { doesUserCouldJoinToRoom } from "../../services/userService/userSimpleService";
import {
  getOneUser,
  updateUser,
} from "../../services/userService/userCrudService";

class JoinRoomHandler implements ISocketHandler {
  public handle({ io, socket, param: name }: ISocketHandlerOptions): void {
    if (!io || !socket || !name) {
      return;
    }

    const user = getOneUser(socket.handshake.query.username as string);

    if (!user || !doesUserCouldJoinToRoom(user)) {
      return;
    }

    const room = getOneRoom(name);

    if (!room || !isAvailableRoom(room)) {
      return;
    }

    const updatedUser = new User({
      ...user,
      currentRoomName: name,
    });

    const roomWithUser = addUserToRoom(room, updatedUser);

    if (!roomWithUser) {
      return;
    }

    updateUser(updatedUser);

    socket.join(name);
    socket.emit("JOIN_TO_ROOM", roomWithUser);
    socket.broadcast.emit("UPDATE_ROOMS", getAvailableRooms());

    io.to(name).emit("UPDATE_ROOM", roomWithUser);
  }
}

export default JoinRoomHandler;
