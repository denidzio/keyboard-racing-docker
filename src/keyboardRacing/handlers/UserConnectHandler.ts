import { ISocketHandler, ISocketHandlerOptions } from "../../socket/interfaces";
import { User } from "../../entities";

import { getAvailableRooms } from "../../services/roomService/roomSimpleService";
import { createUser } from "../../services/userService/userCrudService";

class UserConnectHandler implements ISocketHandler {
  public handle({ socket }: ISocketHandlerOptions): void {
    if (!socket) {
      return;
    }

    const socketQuery = socket.handshake.query.username as string;

    if (!socketQuery) {
      return;
    }

    const user = createUser(new User({ username: socketQuery }));

    if (!user) {
      socket.emit("USERNAME_IS_BUSY");
      socket.disconnect();
      return;
    }

    socket.emit("UPDATE_ROOMS", getAvailableRooms());
  }
}

export default UserConnectHandler;
