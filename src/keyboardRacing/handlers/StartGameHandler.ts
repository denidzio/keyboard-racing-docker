import { texts } from "../../data";
import { SECOND } from "../../constants";
import {
  SECONDS_FOR_GAME,
  SECONDS_TIMER_BEFORE_START_GAME,
} from "../../socket/config";

import { ISocketHandler, ISocketHandlerOptions } from "../../socket/interfaces";

import { Room, Game } from "../../entities";

import { updateRoom } from "../../services/roomService/roomCrudService";

import KeyboardRacingHandlerFactory from "../KeyboardRacingHandlerFactory";
import KeyboardRacingHandlerType from "../KeyboardRacingHandlerType";

import { random } from "../../helpers/random.helper";
import { getAvailableRooms } from "../../services/roomService/roomSimpleService";

class StartGameHandler implements ISocketHandler {
  handle({ io, param: room }: ISocketHandlerOptions): void {
    if (!io || !room) {
      return;
    }

    //CARRYING
    const textId = random(0)(texts.length - 1);
    const updatedRoom = new Room({
      ...room,
      textId,
      hasBeenStartedAt: Date.now(),
    });
    const game = new Game({ textId });

    io.to(updatedRoom.name).emit("START_GAME", game);

    const handlerFactory = new KeyboardRacingHandlerFactory();
    const gameOverHandler = handlerFactory.create(
      KeyboardRacingHandlerType.GAME_OVER
    );

    if (!gameOverHandler) {
      return;
    }

    const timerId = setTimeout(() => {
      gameOverHandler.handle({ io, param: updatedRoom.name });
    }, (SECONDS_FOR_GAME + SECONDS_TIMER_BEFORE_START_GAME) * SECOND);

    updatedRoom.timerId = timerId;
    updateRoom(updatedRoom);

    io.emit("UPDATE_ROOMS", getAvailableRooms());
  }
}

export default StartGameHandler;
