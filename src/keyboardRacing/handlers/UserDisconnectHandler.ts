import { ISocketHandler, ISocketHandlerOptions } from "../../socket/interfaces";

import {
  getOneUser,
  removeUser,
} from "../../services/userService/userCrudService";

import KeyboardRacingHandlerFactory from "../KeyboardRacingHandlerFactory";
import KeyboardRacingHandlerType from "../KeyboardRacingHandlerType";

class UserDisconnectHandler implements ISocketHandler {
  public handle({ io, socket }: ISocketHandlerOptions): void {
    if (!io || !socket) {
      return;
    }

    const user = getOneUser(socket.handshake.query.username as string);

    if (!user) {
      return;
    }

    const handlerFactory = new KeyboardRacingHandlerFactory();
    const leaveRoomHandler = handlerFactory.create(
      KeyboardRacingHandlerType.ROOM_EXIT
    );

    leaveRoomHandler?.handle({ io, socket });
    removeUser(user.username);
  }
}

export default UserDisconnectHandler;
