import { Server, Socket } from "socket.io";
import { DefaultEventsMap } from "socket.io/dist/typed-events";
import KeyboardRacingHandlerFactory from "./KeyboardRacingHandlerFactory";
import KeyboardRacingHandlerType from "./KeyboardRacingHandlerType";

//FACADE
class KeyboardRacingFacade {
  public io: Server<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap>;

  constructor(
    io: Server<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap>
  ) {
    this.io = io;
  }

  listen() {
    const io = this.io;

    if (!io) {
      return;
    }

    const factory = new KeyboardRacingHandlerFactory();

    const addRoomHandler = factory.create(KeyboardRacingHandlerType.ADD_ROOM);
    const userConnectHandler = factory.create(
      KeyboardRacingHandlerType.USER_CONNECT
    );
    const leaveRoomHandler = factory.create(
      KeyboardRacingHandlerType.ROOM_EXIT
    );
    const inputCharHandler = factory.create(
      KeyboardRacingHandlerType.INPUT_CHAR
    );
    const joinRoomHandler = factory.create(
      KeyboardRacingHandlerType.JOIN_TO_ROOM
    );
    const togglePlayerStateHandler = factory.create(
      KeyboardRacingHandlerType.TOGGLE_PLAYER_STATE
    );
    const userDisconnectHandler = factory.create(
      KeyboardRacingHandlerType.USER_DISCONNECT
    );

    io.on("connection", (socket: Socket) => {
      userConnectHandler?.handle({ socket });

      socket.on(KeyboardRacingHandlerType.ADD_ROOM, (param) =>
        addRoomHandler?.handle({ socket, param })
      );
      socket.on(KeyboardRacingHandlerType.JOIN_TO_ROOM, (param) =>
        joinRoomHandler?.handle({ io, socket, param })
      );
      socket.on(KeyboardRacingHandlerType.TOGGLE_PLAYER_STATE, () =>
        togglePlayerStateHandler?.handle({ io, socket })
      );
      socket.on(KeyboardRacingHandlerType.ROOM_EXIT, () => {
        leaveRoomHandler?.handle({ io, socket });
      });
      socket.on(KeyboardRacingHandlerType.INPUT_CHAR, (param) => {
        inputCharHandler?.handle({ io, socket, param });
      });

      socket.on(KeyboardRacingHandlerType.USER_DISCONNECT, () =>
        userDisconnectHandler?.handle({ io, socket })
      );
    });
  }
}

export default KeyboardRacingFacade;
