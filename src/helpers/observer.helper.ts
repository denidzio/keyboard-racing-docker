let handlers = Symbol("handlers");

export const makeObservable = (target: any) => {
  target[handlers] = [];

  target.observe = function (handler: any) {
    this[handlers].push(handler);
  };

  return new Proxy(target, {
    set(target, property, value, receiver) {
      target[handlers].forEach(
        (handler: (arg0: string | symbol, arg1: any) => any) =>
          handler(property, value)
      );

      Reflect.set(target, property, value);
      return true;
    },
  });
};
