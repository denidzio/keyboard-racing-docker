//CARRYING
export const isFieldUniq = (array: any[]) => (field: string) => (value: any) =>
  array.every((item) => item[field] !== value);
