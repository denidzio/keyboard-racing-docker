import { IRoom } from "../entities/interfaces";

export const mapRoomForClient = (room: IRoom) => {
  return { name: room.name, players: room.players };
};
