import { IRoom } from "../entities/interfaces";
import { makeObservable } from "../helpers/observer.helper";
import IObserver from "../interfaces/IObserver";

//PROXY
let rooms = makeObservable([]) as IRoom[] & IObserver;

export const getDb = () => {
  return rooms;
};

export const getAll = (): IRoom[] => {
  return Object.assign([], rooms);
};

export const create = (room: IRoom): IRoom => {
  rooms.push(room);
  return room;
};

export const getOne = (name: string): IRoom | undefined => {
  const room = rooms.find((dbRoom) => dbRoom.name === name);
  return room ? Object.assign({}, room) : undefined;
};

export const update = (room: IRoom): IRoom | undefined => {
  const index = rooms.findIndex((dbRoom) => dbRoom.name === room.name);

  if (index === -1) {
    return;
  }

  rooms[index] = room;
  return Object.assign({}, room);
};

export const remove = (name: string): IRoom | undefined => {
  const index = rooms.findIndex((dbRoom) => dbRoom.name === name);

  if (index === -1) {
    return;
  }

  rooms.splice(index, 1);
  return Object.assign({}, getOne(name));
};
