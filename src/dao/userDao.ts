import { IUser } from "../entities/interfaces";

let users: IUser[] = [];

export const getAll = (): IUser[] => {
  return Object.assign([], users);
};

export const create = (user: IUser): IUser => {
  users = [...users, user];
  return user;
};

export const getOne = (username: string): IUser | undefined => {
  const user = users.find((dbUser) => dbUser.username === username);
  return user ? Object.assign({}, user) : undefined;
};

export const update = (user: IUser): IUser | undefined => {
  if (!users.some((dbUser) => dbUser.username === user.username)) {
    return;
  }

  users = [
    ...users.filter((dbUser) => dbUser.username !== user.username),
    user,
  ];

  return Object.assign({}, user);
};

export const remove = (name: string): IUser | undefined => {
  const user = getOne(name);

  if (!user) {
    return;
  }

  users = users.filter((dbUser) => dbUser.username !== name);
  return Object.assign({}, user);
};
