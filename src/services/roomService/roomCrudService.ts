import { IRoom } from "../../entities/interfaces";

import { getAll, create, getOne, update, remove } from "../../dao/roomDao";
import { isFieldUniq } from "../../helpers/array.helper";

export const getAllRooms = () => {
  return getAll();
};

export const createRoom = (room: IRoom | undefined) => {
  if (!room) {
    return;
  }

  //CARRYING
  if (!isFieldUniq(getAll())("name")(room.name)) {
    return;
  }

  return create(room);
};

export const getOneRoom = (name: string | undefined) => {
  if (!name) {
    return;
  }

  return getOne(name);
};

export const updateRoom = (room: IRoom | undefined) => {
  if (!room) {
    return;
  }

  return update(room);
};

export const removeRoom = (name: string | undefined) => {
  if (!name) {
    return;
  }

  return remove(name);
};
