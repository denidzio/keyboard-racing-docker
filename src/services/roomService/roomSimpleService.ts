import { SECOND } from "../../constants";
import {
  MAXIMUM_USERS_FOR_ONE_ROOM,
  SECONDS_TIMER_BEFORE_START_GAME,
} from "../../socket/config";

import { IPlayer, IRoom, IUser } from "../../entities/interfaces";

import { getCleanPlayers } from "../playerService/playerSimpleService";
import { getAllRooms, getOneRoom, updateRoom } from "./roomCrudService";
import { Player, Room } from "../../entities";

export const getAvailableRooms = () => {
  return getAllRooms().filter(isAvailableRoom);
};

export const updatePlayerInRoom = (
  name: string | undefined,
  player: IPlayer | undefined
) => {
  if (!name || !player) {
    return;
  }

  const room = getOneRoom(name);

  if (!room || !room.players) {
    return;
  }

  const newPlayersList = [
    ...room.players.filter(
      (roomPlayer) => roomPlayer.user.username !== player.user.username
    ),
    player,
  ];

  room.players = newPlayersList;

  return updateRoom(room);
};

export const getRoomWithUser = (user: IUser | undefined) => {
  if (!user) {
    return;
  }

  return getAllRooms().find((room) =>
    room.players.find((dbPlayer) => dbPlayer.user.username === user.username)
  );
};

export const clearRoomTimeout = (room: IRoom | undefined) => {
  if (!room || !room.timerId) {
    return;
  }

  clearTimeout(room.timerId);
};

export const clearRoom = (room: IRoom | undefined) => {
  if (!room) {
    return;
  }

  const cleanPlayers = getCleanPlayers(room.players);

  if (cleanPlayers === undefined) {
    return;
  }

  return updateRoom({ name: room.name, players: cleanPlayers });
};

export const addUserToRoom = (room: IRoom | undefined, user: IUser) => {
  if (!room) {
    return;
  }

  if (!user) {
    return;
  }

  const newPlayer = new Player({
    user,
  });

  const updatedRoom = new Room({
    ...room,
    players: [...room.players, newPlayer],
  });

  return updateRoom(updatedRoom);
};

export const removeUserFromRoom = (room: IRoom | undefined, user: IUser) => {
  if (!room || !user) {
    return;
  }

  const cleanRoom = new Room({
    ...room,
    players: room.players.filter(
      (player) => player.user.username !== user.username
    ),
  });

  return updateRoom(cleanRoom);
};

export const isRoomReady = (room: IRoom | undefined): boolean => {
  if (!room) {
    return false;
  }

  return room.players.every((player) => player.isReady === true);
};

export const isAvailableRoom = (room: IRoom | undefined): boolean => {
  if (!room) {
    return false;
  }

  return (
    !room.hasBeenStartedAt && room.players.length < MAXIMUM_USERS_FOR_ONE_ROOM
  );
};

export const isRoomInGame = (room: IRoom | undefined): boolean => {
  if (!room) {
    return false;
  }

  return !!room.hasBeenStartedAt && room.textId !== undefined;
};

export const isAllowedToInputChar = (room: IRoom | undefined): boolean => {
  if (!room || !room.hasBeenStartedAt) {
    return false;
  }

  return (
    Date.now() - room.hasBeenStartedAt >
    SECONDS_TIMER_BEFORE_START_GAME * SECOND
  );
};
