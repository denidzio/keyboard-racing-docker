import { Express } from "express-serve-static-core";

import gameRoutes from "./gameRoutes";

const router = (app: Express) => {
  app.use("/game", gameRoutes);
};

export default router;
