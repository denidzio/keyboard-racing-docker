import { Router } from "express";

import { texts } from "../data";

const router = Router();

router.get("/texts/:id", (req, res) => {
  const id = Number(req.params.id);

  if (isNaN(id)) {
    return;
  }

  res.send({ text: texts[id] });
});

export default router;
